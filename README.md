## Create container

### # command
```
 sudo lxc-create -n <container-name> -t <template>
```
### # example
```
sudo lxc-create -n mongodb -t ubuntu
```
### # mongodb is the container name and ubuntu is the template
### # list of available templates at /usr/share/lxc/templates
---
## List containers
```
sudo lxc-ls -f
OR
sudo lxc-ls --fancy
```
---
# **Start container**

### # command
## **sudo lxc-start -n <container-name> -d**

### # example
## **sudo lxc-start -n mongodb -d**
### # -d will start contaier as deamon
---
# **View container info**

### # command
## **sudo lxc-info -n <container-name>**

### # example
## sudo lxc-info -n mongodb
---
# **Connect to container**

### # command
## **sudo lxc-connect -n <container-name>**

### # example
## **sudo lxc-connect -n mongodb**
### # It will ask for username/password,
### # default username - ubuntu
### # default passowrd - ubuntu
---
# **Attach to container**

### # command
## **sudo lxc-attach -n <container-name>**

### # example
## **sudo lxc-attach -n mongodb**
---
# **SSH to container**

### # connect as normal ssh session
## **ssh \<user\>@\<container-ip\>**

### # example
## **ssh ubuntu@10.0.3.36**

### # container ip can be find via container information
---
# **Stop container**

### # From host machine
## **sudo lxc-stop -n <container-name>**
## **sudo lxc-stop -n mongodb**

### # From inside container(after connecting via ssh or lxc-console)
## sudo poweroff 
---
# **Delete container**

### # command
## **sudo lxc-destroy -n <container-name>**

### # example
## **sudo lxc-destroy -n mongo**
---
